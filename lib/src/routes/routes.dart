
import 'package:flutter/material.dart';

import 'package:health_check/src/pages/home_page.dart';
import 'package:health_check/src/pages/analisis_page.dart';
import 'package:health_check/src/pages/alert_page.dart';
import 'package:health_check/src/pages/blood_page.dart';
import 'package:health_check/src/pages/animated_container.dart';
import 'package:health_check/src/pages/input_page.dart';
import 'package:health_check/src/pages/nutrition_page.dart';


Map<String, WidgetBuilder> getApplicationRoutes() {

  return <String, WidgetBuilder>{
          '/'                   :  ( BuildContext context ) => HomePage(),
          'analisis'            :  ( BuildContext context ) => AnalisisPage(),
          'blood'               :  ( BuildContext context ) => BloodPage(),
          'alert'               :  ( BuildContext context ) => AlertPage(),
          'nutrition'           :  ( BuildContext context ) => NutritionPage(),
          'animatedContainer'   :  ( BuildContext context ) => AnimatedContainerPage(),
          'inputs'              :  ( BuildContext context ) => InputPage(),
  };

}

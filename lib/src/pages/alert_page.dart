
import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alertas y logros'),
        backgroundColor: Colors.green,
      ),
      body: ListView(
        children: <Widget>[
          _cardTipo1(),
          _cardTipo2(),
          _cardTipo3(),
          _cardTipo4(),
          _cardTipo5(),
          _cardTipo4(),
          _cardTipo1(),
          _cardTipo4(),
          _cardTipo4(),
          _cardTipo5(),
          _cardTipo1(),
          _cardTipo4(),
          _cardTipo5(),
          _cardTipo4(),
          _cardTipo1(),
          _cardTipo5(),
          _cardTipo4(),
          RaisedButton(
            child: Text('Mostrar Alertas'),
            color: Colors.blue,
            textColor: Colors.white,
            shape: StadiumBorder(),
            onPressed:() => _mostrarAlert(context)
          ),
        ],
      ),
    );
  }

  void _mostrarAlert(BuildContext context) {

    showDialog(

      context: context,
      // Para que se pueda cerrar la alerta haciendo click afuera
      barrierDismissible: false,
      builder: (context) {

        return AlertDialog(

          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: Text('Hola! Esto es una alerta'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('Siempre que cumplas algun logro, o Health Check detecte alguna anomalía en tu cuerpo, aperecerá en tu móvil una ventana emergente como esta. '),
              // FlutterLogo(
              //   size: 200.0,
              // ),
              SizedBox(height: 40.0,),
              Icon(Icons.error_outline,size: 150.0,color: Colors.green,)
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () => Navigator.of(context).pop(),
            ), 
            FlatButton(
              child: Text('Ok'),
              onPressed: (){
                Navigator.of(context).pop();
              }
            ), 
          ],

        );

      }

    );
  }

  Widget _cardTipo1() {

    return Card(
      //Para las sombras
      elevation: 2.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
             leading: Icon(Icons.sentiment_dissatisfied, color: Colors.red,),
              title: Text('Niveles de agua muy bajos'),
              subtitle: Text('Su cuerpo requiere actualmente 1,2 litros de agua para su correcto funcionamiento. Puede que experimente sintomas como sequedad, falta de concentración. En caso de deshidratación continuada, posibles mareos y hasta la muerte.'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Descartar'),
                onPressed: (){}, 
              ),
              FlatButton(
                child: Text('Fijar'),
                onPressed: (){}, 
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2() {

    return Card(
      //Para las sombras
      elevation: 2.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
             leading: Icon(Icons.check_box, color: Colors.green,),
              title: Text('Enhorabuena!!! Llevas una racha de 7 dias comiendo saludable'),
              subtitle: Text('Tu cuerpo te lo agradecera en el futuro'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Descartar'),
                onPressed: (){}, 
              ),
              FlatButton(
                child: Text('Fijar'),
                onPressed: (){}, 
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo3() {

    return Card(
      //Para las sombras
      elevation: 2.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
             leading: Icon(Icons.check_box, color: Colors.green,),
              title: Text('Enhorabuena!!! Llevas una racha de 5 dias comiendo saludable'),
              subtitle: Text('Tu cuerpo te lo agradecera en el futuro'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Descartar'),
                onPressed: (){}, 
              ),
              FlatButton(
                child: Text('Fijar'),
                onPressed: (){}, 
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo4() {

    return Card(
      //Para las sombras
      elevation: 2.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
             leading: Icon(Icons.check_box, color: Colors.green,),
              title: Text('Enhorabuena!!! Llevas una racha de 3 dias comiendo saludable'),
              subtitle: Text('Tu cuerpo te lo agradecera en el futuro'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Descartar'),
                onPressed: (){}, 
              ),
              FlatButton(
                child: Text('Fijar'),
                onPressed: (){}, 
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo5() {

    return Card(
      //Para las sombras
      elevation: 2.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
             leading: Icon(Icons.sentiment_dissatisfied, color: Colors.red,),
              title: Text('Vaya!, parece que has tenido las pulsaciones muy altas.'),
              subtitle: Text('Sus pulsaciones han superado el umbral recomendado. Confirme si esta en reposo y avisaré a un medico por usted.'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Descartar'),
                onPressed: (){}, 
              ),
              FlatButton(
                child: Text('Fijar'),
                onPressed: (){}, 
              )
            ],
          )
        ],
      ),
    );
  }

}
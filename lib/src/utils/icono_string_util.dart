

import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  
  'healing'       : Icons.healing,
  'blood'         : Icons.local_hospital,
  'add_alert'     : Icons.add_alert,
  'nutrition'     : Icons.restaurant_menu,
  'folder_open'   : Icons.folder_open,
  'donut_large'   : Icons.donut_large,
  'input'         : Icons.input,
  'tune'          : Icons.tune,
  'list'          : Icons.list,

};


Icon getIcon(String nombreIcono){
  
  return Icon( _icons[nombreIcono], color: Colors.blue ); 
}
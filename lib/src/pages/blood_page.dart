
import 'package:flutter/material.dart';

class BloodPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Análisis de sangre'),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://image.slidesharecdn.com/resultadosdeanalisisdesangre-140331074136-phpapp01/95/resultados-de-analisis-de-sangre-1-638.jpg?cb=1396251715'),
          placeholder: AssetImage('assets/jar-loading.gif'),
          fadeInDuration: Duration(seconds: 3),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
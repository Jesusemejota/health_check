
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';

class AnalisisPage extends StatefulWidget {
  @override
  _AnalisisPageState createState() => _AnalisisPageState();
}

class _AnalisisPageState extends State<AnalisisPage> {

  int _ritmoCardiaco = 0;
  int _nivelAgua = 0;
  int _oxigenoSangre = 0;
  int _nivelEstres = 0;
  String _estadoActual = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Análisis de salud'),
        backgroundColor: Colors.green,
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          _cardTipo2(),
          _cardTipo3(),
          _cardTipo4(),
          _cardTipo5(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        backgroundColor: Colors.green,
        onPressed: _cambioParametros,
      ),
    );
  }

  Widget _cardTipo1() {

    return Card(
      //Para las sombras
      elevation: 10.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.favorite, color: Colors.red,),
              title: Text('RITMO CARDIACO         $_ritmoCardiaco puls/min'),
          ),
        ],
      ),
    );
  }

  Widget _cardTipo2() {

    return Card(
      //Para las sombras
      elevation: 10.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.local_drink, color: Colors.blue,),
              title: Text('NIVEL DE AGUA               $_nivelAgua %'),
          ),
        ],
      ),
    );
  }

  Widget _cardTipo3() {

    return Card(
      //Para las sombras
      elevation: 10.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.pie_chart_outlined, color: Colors.black,),
              title: Text('OXIGENO EN SANGRE    $_oxigenoSangre %'),
          ),
        ],
      ),
    );
  }

  Widget _cardTipo4() {

    return Card(
      //Para las sombras
      elevation: 10.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.sentiment_neutral, color: Colors.orange,),
              title: Text('NIVEL DE ESTRES            $_nivelEstres/10'),
          ),
        ],
      ),
    );
  }

  Widget _cardTipo5() {

    return Card(
      //Para las sombras
      elevation: 10.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
              leading: Icon(Icons.local_hospital, color: Colors.green,),
              title: Text('ESTADO DE SALUD         $_estadoActual'),
          ),
        ],
      ),
    );
  }


  void _cambioParametros() {

    final random = Random();

    setState(() {
      sleep(
        Duration(seconds: 1)
      );
      _ritmoCardiaco  = 48 + random.nextInt(100);
      _nivelEstres = random.nextInt(10);
      _nivelAgua = 38 + random.nextInt(45);
      _oxigenoSangre = 78 + random.nextInt(20);
      if(_ritmoCardiaco < 50 || _ritmoCardiaco > 140 || _nivelEstres > 8 || _nivelAgua < 40 || _oxigenoSangre < 80)
        _estadoActual = 'Muy grave';
      else if(_ritmoCardiaco < 60 || _ritmoCardiaco > 100 || _nivelEstres > 5 || _nivelAgua < 50 || _oxigenoSangre < 85)
        _estadoActual = 'Malo';
      else if(_ritmoCardiaco > 60 && _ritmoCardiaco < 70 && _nivelEstres < 3 && _nivelAgua > 60 && _oxigenoSangre > 95)
        _estadoActual = 'Muy bueno';
      else if(_ritmoCardiaco > 60 && _ritmoCardiaco < 100 && _nivelEstres < 5  && _nivelAgua > 50 && _oxigenoSangre > 85)
        _estadoActual = 'Bueno';

    });

  }
}
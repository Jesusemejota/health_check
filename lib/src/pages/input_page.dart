
import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {

  String _nombre = '';
  String _email  = '';
  String _password = '';
  String _fecha = '';

  String _opcionSelecionada = 'Persona común';

  List<String> _roles = ['Médico', 'Deportista de elite', 'Persona común', 'Persona con problemas de salud'];

  TextEditingController _inputFieldDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registro de datos'),
        backgroundColor: Colors.green,
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearFecha(context),
          Divider(),
          _crearDropdown(),
          Divider(
                color: Colors.green,
                indent: 10.0,
                endIndent: 10.0,
                thickness: 0.65,
          ),
          _crearPersona(),
        ],
      ),
    );
  }

  Widget _crearInput() {

    return TextField(
      // Para que se ponga automaticamente el foco
      // autofocus: true,
      // Para arreglar la capitalizaciOn
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        counter: Text('Caracteres: ${_nombre.length}'),
        // Texto de sugerencia para que tenga una idea de lo que vamos a poner ahí
        hintText: 'Nombre de la persona',
        labelText: 'Nombre',
        helperText: 'Más de 4 caracteres',
        suffixIcon: Icon(Icons.accessibility),
        icon: Icon(Icons.account_circle),
      ),
      onChanged: (valor){
        setState(() {
          _nombre = valor;
        });
      },
    );

  }

  Widget _crearEmail() {

    return TextField(
      
      // Para que sea email
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        // counter: Text('Letras ${_nombre.length}'),
        // Texto de sugerencia para que tenga una idea de lo que vamos a poner ahí
        hintText: 'Email',
        labelText: 'Email',
        suffixIcon: Icon(Icons.alternate_email),
        icon: Icon(Icons.email),
      ),
      onChanged: (valor){
        setState(() {
          _email = valor;
        });
      },
    );

  }

  Widget _crearPassword() {

    return TextField(
      
      // Para que sea una password
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        // Texto de sugerencia para que tenga una idea de lo que vamos a poner ahí
        hintText: 'Password',
        labelText: 'Password',
        suffixIcon: Icon(Icons.remove_red_eye),
        icon: Icon(Icons.lock),
      ),
      onChanged: (valor){
        setState(() {
          _password = valor;
        });
      },
    );

  }

  Widget _crearFecha(BuildContext context) {

    return TextField(
      
      // Para que las personas no puedan copiar su contenido
      enableInteractiveSelection: false,
      // Para que cuando se selecione la fecha se escibra en el field
      controller: _inputFieldDateController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0)
        ),
        // counter: Text('Letras ${_nombre.length}'),
        // Texto de sugerencia para que tenga una idea de lo que vamos a poner ahí
        hintText: 'Fecha de nacimiento',
        labelText: 'Fecha de nacimiento',
        suffixIcon: Icon(Icons.date_range),
        icon: Icon(Icons.calendar_today),
      ),
      onTap: (){

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);

      },
    );

  }

  _selectDate(BuildContext context) async {

    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(1950),
      lastDate: new DateTime(2025),
      // locale: Locale('es', 'ES')
    );

    if (picked != null) {
      setState(() {
        _fecha = picked.toString();
        _inputFieldDateController.text = _fecha;
      });
    }

  }

  List<DropdownMenuItem<String>> getOpcionesDropdown() {

    List<DropdownMenuItem<String>> lista = new List();

    _roles.forEach((roles) { 

      lista.add(DropdownMenuItem(
        child: Text(roles),
        value: roles,
      ));

    });

    return lista;
  }

  Widget _crearDropdown() {

    return Row(
      children: <Widget>[
        Icon(Icons.clear_all),
        SizedBox(width: 30.0,),
        Expanded(
          child: DropdownButton(
            value: _opcionSelecionada,
            items: getOpcionesDropdown(), 
            onChanged: (opt) {
              setState(() {
                _opcionSelecionada = opt;
              });
            },
          ),
        )
      ],
    );
    
    
    
  }

  Widget _crearPersona() {

    return ListTile(
      title: Text('Sus datos son los siguientes:'),
      subtitle: Text('$_nombre $_email'),
      trailing: Text(_opcionSelecionada),
      
    );

  }



}
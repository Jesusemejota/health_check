
import 'package:flutter/material.dart';

import 'package:health_check/src/routes/routes.dart';

import 'package:health_check/src/pages/analisis_page.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HealthCheck App',
      debugShowCheckedModeBanner: false,
      // home: HomePage(),
      initialRoute: '/',
      routes: getApplicationRoutes(),
      
      //En caso de que la ruta no se encuentre nos llevara a la pAgina de anAlisis
      onGenerateRoute: ( RouteSettings settings ) {

        print('Ruta llamada: ${settings.name}' );

        return MaterialPageRoute(
          builder: (BuildContext context) => AnalisisPage()
        );
      },

    );
  }
}

import 'package:flutter/material.dart';

import 'package:health_check/src/providers/menu_provider.dart';

import 'package:health_check/src/utils/icono_string_util.dart';



class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Health Check'),
        backgroundColor: Colors.green,
        actions: <Widget>[

          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('JE'),
              backgroundColor: Colors.brown,
              radius: 20.0,
            ),
          ),

          // Container(
          //   padding: EdgeInsets.all(5.0),
          //   child: CircleAvatar(
          //      backgroundImage: NetworkImage('https://vignette.wikia.nocookie.net/marvelcinematicuniverse/images/8/87/Stan_Lee.png/revision/latest?cb=20190303192815&path-prefix=es'),
          //      radius: 25.0,
          //   ),
          // ),
  
        ],
      ),
      body: _lista(),
    );
  }

  Widget _lista() {

    // menuProvider.cargarData();
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot ){

        return ListView(
          children: _listaItems( snapshot.data, context ),
        );

      },

    );

    //return ListView(
    //  children: _listaItems(),
    //);

   }

  List<Widget>_listaItems( List<dynamic> data, BuildContext context) {

    final List<Widget> opciones = [];

    if(data==null)
      return [];

    data.forEach((opt) { 

      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        leading: getIcon( opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.green,),
        onTap: (){
          
          Navigator.pushNamed(context, opt['ruta']);

        },
      );

      opciones..add(widgetTemp)
              ..add(Divider(
                color: Colors.green,
                indent: 10.0,
                endIndent: 10.0,
                thickness: 0.65,
              ));

    });

    return opciones;

  }
}
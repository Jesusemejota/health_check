
import 'package:flutter/material.dart';

class NutritionPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nutricion'),
        backgroundColor: Colors.green,
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          // _cardTipo1(),
          // SizedBox(height: 30.0),
          _cardTipo4(),
          SizedBox(height: 30.0),
          _cardTipo2(),
          SizedBox(height: 30.0),
          _cardTipo4(),
          SizedBox(height: 30.0),
          _cardTipo3(),
          SizedBox(height: 30.0),
          _cardTipo4(),
          SizedBox(height: 30.0),
          _cardTipo5(),
          SizedBox(height: 30.0),
          _cardTipo4(),
          SizedBox(height: 30.0),
          _cardTipo6(),
          SizedBox(height: 30.0),
        ],
      ),
    );
  }

  Widget _cardTipo1() {

    return Card(
      //Para las sombras
      elevation: 10.0,
      //Para la redondez de los bordes
      shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0)),

      child: Column(
        children: <Widget>[
          ListTile(
             leading: Icon(Icons.sentiment_dissatisfied, color: Colors.red,),
              title: Text('Niveles de agua muy bajos'),
              subtitle: Text('Su cuerpo requiere actualmente 1,2 litros de agua para su correcto funcionamiento. Puede que experimente sintomas como sequedad, falta de concentración. En caso de deshidratación continuada, posibles mareos y hasta la muerte.'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: (){}, 
              ),
              FlatButton(
                child: Text('Ok'),
                onPressed: (){}, 
              )
            ],
          )
        ],
      ),
    );

  }

  Widget _cardTipo2() {

    final card = Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[

          // Para añadirle un loading
          FadeInImage(
            image: NetworkImage('https://recetasveganas.net/wp-content/uploads/2017/01/porridge_avena_vegano_fruta-3.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            // Para que la imagen se adapte a todo el ancho posible
            fit: BoxFit.cover,
          ),

          //Image(
          //  image: NetworkImage('https://www.tom-archer.com/wp-content/uploads/2018/06/milford-sound-night-fine-art-photography-new-zealand.jpg'),
          //),

          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Desayuno de avena con frutas')
          )
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 10.0)
          )
        ]
      ),
      //Cortar cualquier cosa que se encuentre fuera del contenedor
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );

  }

  Widget _cardTipo3() {

    final card = Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[

          // Para añadirle un loading
          FadeInImage(
            image: NetworkImage('https://static2.diariosur.es/www/multimedia/202001/22/media/cortadas/fruta-temporada-k6lF-U901309424433R0H-1248x770@Diario%20Sur.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            // Para que la imagen se adapte a todo el ancho posible
            fit: BoxFit.cover,
          ),

          //Image(
          //  image: NetworkImage('https://www.tom-archer.com/wp-content/uploads/2018/06/milford-sound-night-fine-art-photography-new-zealand.jpg'),
          //),

          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Almuerzo con varias frutas a elegir')
          )
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 10.0)
          )
        ]
      ),
      //Cortar cualquier cosa que se encuentre fuera del contenedor
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );

  }

  Widget _cardTipo4() {

    final card = Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[

          // Para añadirle un loading
          FadeInImage(
            image: NetworkImage('https://files.alerta.rcnradio.com/alerta_bogota/public/2020-03/vaso_de_agua_0.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 275.0,
            // Para que la imagen se adapte a todo el ancho posible
            fit: BoxFit.cover,
          ),

          //Image(
          //  image: NetworkImage('https://www.tom-archer.com/wp-content/uploads/2018/06/milford-sound-night-fine-art-photography-new-zealand.jpg'),
          //),

          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Agua antes de cada comida')
          )
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 10.0)
          )
        ]
      ),
      //Cortar cualquier cosa que se encuentre fuera del contenedor
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }
   
  Widget _cardTipo5() {

    final card = Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[

          // Para añadirle un loading
          FadeInImage(
            image: NetworkImage('https://i.ytimg.com/vi/xP7ZaWkEz3A/maxresdefault.jpg'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 275.0,
            // Para que la imagen se adapte a todo el ancho posible
            // fit: BoxFit.cover,
          ),

          //Image(
          //  image: NetworkImage('https://www.tom-archer.com/wp-content/uploads/2018/06/milford-sound-night-fine-art-photography-new-zealand.jpg'),
          //),

          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Proporciones de una comida saludable')
          )
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 10.0)
          )
        ]
      ),
      //Cortar cualquier cosa que se encuentre fuera del contenedor
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

  Widget _cardTipo6() {

    final card = Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[

          // Para añadirle un loading
          FadeInImage(
            image: NetworkImage('https://i1.wp.com/foodcoach.es/wp-content/uploads/2019/07/Cenas-saludables.jpg?fit=1920%2C1080&ssl=1'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 275.0,
            // Para que la imagen se adapte a todo el ancho posible
            fit: BoxFit.cover,
          ),

          //Image(
          //  image: NetworkImage('https://www.tom-archer.com/wp-content/uploads/2018/06/milford-sound-night-fine-art-photography-new-zealand.jpg'),
          //),

          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Cenas saludables')
          )
        ],
      ),
    );

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 10.0)
          )
        ]
      ),
      //Cortar cualquier cosa que se encuentre fuera del contenedor
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

}
